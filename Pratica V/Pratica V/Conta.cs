﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pratica_V
{
    class Conta
    {
        private double saldo;
        private string nome;
        public string id;

        public void CriarConta(string nome, string id)
        {
            this.saldo = 0;
            this.nome = nome;
            this.id = id;
        }

        public double Saldo()
        {
            return this.saldo;
        }

        public void Depositar(double valor)
        {
            this.saldo += valor;
        }

        public void Sacar(double valor)
        {
            this.saldo -= valor;
        }
    }
}
