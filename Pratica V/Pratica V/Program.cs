﻿using System;
using System.Collections.Generic;

namespace Pratica_V
{
    class Program
    {
        static List<Conta> contas = new List<Conta>();
        static void Main(string[] args)
        {
            bool y = true;
            do
            {
                string operaçao;
                Console.Clear();
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("|  Operação Desejada   |   Numero a ser digitado   |");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("|     Criar Conta      |             1             |");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("|       Extrato        |             2             |");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("|        Saque         |             3             |");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("|       Deposito       |             4             |");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("|         Sair         |             0             |");
                Console.WriteLine("----------------------------------------------------");

                Console.Write("Digite a operação desejada: ");
                operaçao = Console.ReadLine();

                switch (operaçao)
                {
                    case "1": //Criar Conta
                        {
                            Conta c1 = new Conta();
                            string nome, id;
                            Console.Clear();
                            Console.WriteLine("CRIAR CONTA");

                            Console.WriteLine("Digite seu nome: ");
                            nome = Console.ReadLine();
                            Console.WriteLine("Digite seu id: ");
                            id = Console.ReadLine();

                            c1.CriarConta(nome, id);

                            contas.Add(c1);


                            Console.WriteLine("aperte uma tecla para continuar");
                            Console.ReadKey();
                        }
                        break;
                    case "2": //Extrato
                        {
                            string id;
                            bool aux = true;
                            Console.Clear();
                            Console.WriteLine("Digite o id: ");
                            id = Console.ReadLine();
                            foreach (Conta c in contas)
                            {
                                if (c.id == id)
                                {
                                    aux = false;
                                    Console.WriteLine(c.Saldo() + " reais");
                                    break;
                                }
                            }
                            if (aux)
                                Console.WriteLine("Conta nao encontrada");

                            Console.WriteLine();
                            Console.WriteLine("aperte uma tecla para continuar");
                            Console.ReadKey();
                        }
                        break;
                    case "3": //Saque
                        {
                            string id;
                            bool aux = true;
                            double saque;
                            Console.Clear();


                            Console.WriteLine("Digite o id: ");
                            id = Console.ReadLine();
                            foreach (Conta c in contas)
                            {
                                if (c.id == id)
                                {
                                    aux = false;
                                    Console.Write("Digite o valor a ser sacado: ");
                                    saque = Convert.ToDouble(Console.ReadLine());

                                    if (c.Saldo() >= saque)
                                    {
                                        c.Sacar(saque);
                                        Console.WriteLine("saque de {0} reais feito com sucesso!", saque);
                                    }
                                    else
                                    {
                                        Console.WriteLine("Voce nao pode sacar esse valor, saldo insuficiente");
                                        Console.WriteLine("o seu saldo é de {0} reais", c.Saldo());
                                    }
                                    break;
                                }
                            }
                            if (aux)
                                Console.WriteLine("Conta nao encontrada");


                            Console.WriteLine("aperte uma tecla para continuar");
                            Console.ReadKey();
                        }
                        break;
                    case "4": //Deposito
                        {
                            string id;
                            bool aux = true;
                            Console.Clear();

                            Console.WriteLine("Digite o id: ");
                            id = Console.ReadLine();
                            foreach (Conta c in contas)
                            {
                                if (c.id == id)
                                {
                                    aux = false;
                                    Console.Write("Digite o valor a ser depositado: ");
                                    c.Depositar(Convert.ToDouble(Console.ReadLine()));

                                    break;
                                }
                            }
                            if (aux)
                                Console.WriteLine("Conta nao encontrada");
                            Console.WriteLine("aperte uma tecla para continuar");
                            Console.ReadKey();
                        }
                        break;
                    case "0": //sair
                        {
                            Console.WriteLine("Voce saiu!");
                            y = false;
                        }
                        break;
                    default:
                        {
                            Console.Clear();
                            Console.WriteLine("Operação incorreta!");
                            Console.WriteLine("aperte uma tecla para continuar");
                            Console.ReadKey();
                        }
                        break;
                }
            } while (y == true);
        }
    }
}
